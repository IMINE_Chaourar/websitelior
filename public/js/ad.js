$('#add_image').click(
			function() {

				//je recupere le numero des futurs champs que je veux creer
				const index = +$('#widgets-counter').val();

				//je recupere le prototype des entries
				const tmpl = $('#ad_images').data('prototype').replace(
						/__name__/g, index);

				//J'injecte ce code au sein de la div
				$('#ad_images').append(tmpl);


					$('#widgets-counter').val(index+1);
				
				//je gere le boutton supprimer
				handleDeleteButtons();
			});



	function handleDeleteButtons(){
		$('button[data-action="delete"]').click(function(){
			const target=this.dataset.target;
			$(target).remove();

			
			});
		}



	function updateCounter(){
		const count=+$('#ad_images div.form-group').length;
		$('#widgets-counter').val(count)
		}

	updateCounter();
	handleDeleteButtons();