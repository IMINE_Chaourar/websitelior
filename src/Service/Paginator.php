<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class Paginator
{
    private $entityClass;
    private $limit = 10;
    private $curentPage = 1;
    private $manager;
    private $twig;

    public function __construct(EntityManagerInterface $manager,Environment $twig ){
        $this->manager=$manager;
        $this->twig=$twig;
    }


    public function display(){
        $this->twig->display('admin/partials/pagination.html.twig',[
            ''
        ]);
    }



    public function getData(){
        // calculer offset debut
        $offset=$this->curentPage * $this->limit -$this->limit;

        //demander au repository de trouver les elements
        $repo=$this->manager->getRepository($this->entityClass);
        $data = $repo->findBy([],[],$this->limit,$offset);

        //envoyer le resultat
        return $data;
    }

    public function getPages(){
        //connaitre le totale des enristrement
        $total = count($this->manager->getRepository($this->entityClass)->findAll());
        
        //faire la division,l'arrondie et l'envoyer 
        $pages= ceil( $total / $this->limit );

        return $pages;
    }

    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    public function getEntityClass()
    {

        return $this->entityClass;
    }


    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit()
    {

        return $this->limit;
    }

    public function setCurentPage($curentPage)
    {
        $this->curentPage = $curentPage;
        return $this;
    }

    public function getCurantPage()
    {

        return $this->curentPage;
    }
}
