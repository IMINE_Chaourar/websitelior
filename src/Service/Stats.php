<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class Stats{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

     //getSingleScalarResult => returner un scalaire 
     //getResulet => retourne un tbleau dans un autre tableau

    public function getUsersCount(){
        return $this->manager->createQuery('SELECT COUNT(u) FROM App\Entity\User u')->getSingleScalarResult();
    }
    
    public function getAdsCount(){
        return $this->manager->createQuery('SELECT COUNT(a) FROM App\Entity\Ad a')->getSingleScalarResult();
    }

    public function getCommentsCount(){
        return $this->manager->createQuery('SELECT COUNT(c) FROM App\Entity\Comment c')->getSingleScalarResult();
    }

    public function getBookingsCount(){
        return $this->manager->createQuery('SELECT COUNT(b) FROM App\Entity\Booking b')->getSingleScalarResult();
    }

    public function getStats(){
       $ads=$this->getAdsCount();
       $bookings=$this->getBookingsCount();
       $comments=$this->getCommentsCount();
       $users=$this->getUsersCount();

       return compact('bookings','users','ads','comments');//compact permet de creer un tableau telque les clés ==valeurs;
    }

    public function getAdsStats($direction){
        return $this->manager->createQuery('SELECT AVG(c.rating) as note,a.title,u.firstName,u.lastName,u.picture
        FROM App\Entity\Comment c
        JOIN c.ad a
        JOIN a.author u
        GROUP BY a
        ORDER BY note  '.$direction)->setMaxResults(5)->getResult(); 


    }

   

}