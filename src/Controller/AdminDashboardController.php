<?php

namespace App\Controller;

use App\Service\Stats;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function index(EntityManagerInterface $manager,Stats $stats){
       
        return $this->render('admin/dashboard/index.html.twig', [
            'stats'=>$stats->getStats(),
            'bestAds'=>$stats->getAdsStats('ASC'),
            'worstAds'=>$stats->getAdsStats('DESC')
        ]);
    }
}
