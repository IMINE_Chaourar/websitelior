<?php
namespace App\Controller;

use App\Entity\Ad;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AdRepository;
use App\Form\AdType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Image;

class AdController extends AbstractController
{

    /**
     *
     * @Route("/ads", name="ad_index")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repo->findAll();
        return $this->render('ad/index.html.twig', [
            'ads' => $ads
        ]);
    }

    /**
     * permet de creer une annonce
     *
     * @Route("/ads/new",name="ads_create")
     * @IsGranted("ROLE_USER")
     * 
     */
    public function create(Request $request, EntityManagerInterface $manager)
    {
        $ad = new Ad();
        
        
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($ad->getImages() as $image){
                $image->setAd($ad);
                $manager->persist($image);
            }
            
            $ad->setAuthor($this->getUser());
            
            $manager->persist($ad);
            $manager->flush();
            $this->addFlash('success', "L'annonce <strong>" . $ad->getTitle() . "</strong> a ete creer avec succes");

            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/newad.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    
    /**
     * permet d'editer une annonce
     * @Route("/ads/{slug}/edit",name="ads_edit")
     * @IsGranted("ROLE_USER")
     * @Security("is_granted('ROLE_USER') and user===ad.getAuthor()",
     * message="Cette annonce n'est pas la votre donc vous ne pouvez pas la modifier")
     */
    public function editAd(Request $request,Ad $ad,EntityManagerInterface $manager){
        
        $form=$this->createForm(AdType::class,$ad);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($ad->getImages() as $image){
                $image->setAd($ad);
                $manager->persist($image);
            }
            
            $manager->persist($ad);
            $manager->flush();
            $this->addFlash('success', "L'annonce <strong>" . $ad->getTitle() . "</strong> a ete modifier avec succes");
            
            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }
        
        
        
        return $this->render('/ad/editad.html.twig',[
            'form'=>$form->createView()
        ]);
    }
    
    
    /**
     * permet de supprimer une annonce
     * @Route("/ads/{slug}/delete",name="ads_delete")
     * @IsGranted("ROLE_USER")
     * @Security("is_granted('ROLE_USER') and user===ad.getAuthor()",
     * message="Cette annonce n'est pas la votre donc vous ne pouvez pas la supprimer")
     */
    public function deleteAd(EntityManagerInterface $manager,Ad $ad){
        
        $manager->remove($ad);
        $manager->flush();
        $this->addFlash("success", "Vous avez suprimé l'annonce ".$ad->getTitle());
        
        return $this->redirectToRoute("ad_index");
        
    }
    
    
    

    /**
     * permet d'afficher une annonce
     *
     * @Route("/ads/{slug}",name="ads_show")
     */
    public function showAd(Ad $ad)
    {
        // $ad=$repo->findOneBySlug($slug);
        return $this->render('/ad/show.html.twig', [
            'ad' => $ad
        ]);
    }
}
