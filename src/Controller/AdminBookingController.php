<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\AdminBookingType;
use App\Repository\BookingRepository;
use App\Service\Paginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminBookingController extends AbstractController
{
    /**
     * @Route("/admin/bookings/{page}", name="admin_booking",requirements={"page":"\d+"})
     */
    public function index(BookingRepository $repo,$page=1,Paginator $paginator)
    {
        // $limit=10;
        // $start=$page*$limit-$limit;
        // //      1*10-10=0
        // //      3*10-10=20
        $paginator->setEntityClass(Booking::class)
                ->setCurentPage($page);
                

         $bookings=$paginator->getData();        
        // $total=count( $repo->findAll() );
        // $pages=ceil( $total/10 );//3.2=4
        $pages=$paginator->getPages();
        
        return $this->render('admin/booking/index.html.twig', [
            'bookings'=>$bookings,
            'pages'=>$pages,
            'page'=>$page//parametre de la route
        ]);
    }

    /**
     * @Route("/admin/bookings/{id}/edit",name="admin_booking_edit")
     */
    public function edit(Booking $booking, EntityManagerInterface $manager, Request $request)
    {
        $form=$this->createForm(AdminBookingType::class,$booking,[
            'validation_groups'=>["Default"]
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $booking->setAmount(0);
            $manager->persist($booking);
            $manager->flush();
            $this->addFlash("success","La reservation a ete modifie avec succes");
            return $this->redirectToRoute("admin_booking");
        }

        return $this->render('admin/booking/edit.html.twig',[
            'form'=>$form->createView(),
            'booking'=>$booking
        ]);
    }

    /**
     * @Route("/admin/bookings/{id}/delete",name="admin_booking_delete")
     */
    public function delete(Booking $booking,EntityManagerInterface $manager){
        $manager->remove($booking);
        $manager->flush();
        $this->addFlash("success","La reservation a ete supprimé avec succes");

        return $this->redirectToRoute("admin_booking");
    }
}
