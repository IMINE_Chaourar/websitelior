<?php
namespace App\Controller;

use App\Repository\AdRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class TestController extends AbstractController
{
    
    /**
     * @Route("/",name="accueil")
     */
    public function hello(AdRepository $repo,UserRepository $repo1){
        return $this->render('/hello.html.twig',[
            'ads'=>$repo->findBestAds(3),
            'users'=>$repo1->findBestUsers(2)
        ]);
    }
}

