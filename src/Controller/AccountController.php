<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\AccountType;
use App\Entity\PasswordUpdate;
use App\Form\PasswordUpdateType;
use Symfony\Component\Form\FormError;

class AccountController extends AbstractController
{

    /**
     *
     * @Route("/login", name="account_login")
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        return $this->render('account/login.html.twig', [
            'controller_name' => 'AccountController',
            'hasError' => $error !== null,
            'username' => $username
        ]);
    }

    /**
     * permet de deconnecter
     *
     * @Route("/logout",name="account_logout")
     */
    public function logout()
    {
        return $this->render('account/login.html.twig', []);
    }

    /**
     * permet d'afficher le formulaire d'inscription des utilisateurs
     *
     * @Route("/register",name="account_register")
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getHash());
            $user->setHash($hash);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', "Votre compte a été cree avec succes");

            return $this->redirectToRoute("account_login");
        }

        return $this->render('account/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * permet de modifier un profil
     *
     * @Route("/account/profile",name="account_profile")
     * @IsGranted("ROLE_USER")
     * 
     */
    public function profile(Request $request, EntityManagerInterface $manager)
    {
        $user = $this->getUser();
        $form = $this->createForm(AccountType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();
            $this->addFlash("success", "Le profil a été modifie avec succes");
        }

        return $this->render('/account/profile.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *permet de modifier le password
     * @Route("/account/update-password",name="account_password")
     * @IsGranted("ROLE_USER")
     */
    public function updatePassword(UserPasswordEncoderInterface $encoder, Request $request, EntityManagerInterface $manager)
    {
        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // verifier que le oldPassword du formulaire
            if (! password_verify($passwordUpdate->getOldPassword(), $user->getHash())) {
                // gerer l'erreur
                $form->get("oldPassword")->addError(new FormError("Le mot de passe tapez est faux !!"));
            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPassword);
                $user->setHash($hash);
                $manager->persist($user);
                $manager->flush();

                $this->addFlash("success", "Le mot de passe a ete mofifie avec succes");
                return $this->redirectToRoute("accueil");
            }
        }

        return $this->render('/account/password.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * permet de voir mon compte
     * @Route("/account",name="account_index")
     * @IsGranted("ROLE_USER")
     */
    public function myAccount(){
        
        return $this->render('user/index.html.twig',[
            'user'=>$this->getUser()
        ]);
    }
    
    
    /**
     * permet d'afficher toutes les reservation de ce compte
     * @Route("/accounts/bookings",name="account_bookings")
     */
    public function bookings(){
        
        return $this->render('account/bookings.html.twig');
    }
  
    
}
