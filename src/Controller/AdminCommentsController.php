<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Comment;
use App\Form\CommentAdminType;
use App\Repository\CommentRepository;
use App\Service\Paginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class AdminCommentsController extends AbstractController
{
    /**
     * @Route("/admin/comments/{page}", name="admin_comments",requirements={"page":"\d+"})
     */
    public function index(CommentRepository $repo,Paginator $paginator,$page=1)
    {
        $paginator->setEntityClass(Comment::class)
        ->setCurentPage($page);
        

        $comments=$paginator->getData();        
        
        $pages=$paginator->getPages();

        return $this->render('admin/comments/index.html.twig', [
            'comments'=>$comments,
            'pages'=>$pages,
            'page'=>$page//parametre de la route
        ]);
    }

    /**
     * @Route("/admin/comments/{id}", name="admin_comments_delete")
     */
    public function delete(Comment $comment,EntityManagerInterface $manager){
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash("success","Le commentaire a ete supprimé avec succes");
        return $this->redirectToRoute("admin_comments");
    }



    /**
     * @Route("/admin/comments/{id}/edit", name="admin_comments_edit")
     */
    public function edit(Comment $comment,EntityManagerInterface $manager,Request $request){
        $form=$this->createForm(CommentAdminType::class,$comment);
        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid() ){
            $manager->persist($comment);
            $manager->flush();
            
            $this->addFlash("success", "Le commentaire a ete modifié avec success");
        }

        return $this->render('admin/comments/editComment.html.twig',[
            'form'=>$form->createView(),
            'comment'=>$comment
        ]);
    }

}
