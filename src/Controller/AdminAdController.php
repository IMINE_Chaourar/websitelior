<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AdRepository;
use App\Entity\Ad;
use App\Entity\Booking;
use App\Form\AdType;
use App\Service\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class AdminAdController extends AbstractController
{
    /**
     * @Route("/admin/ads/{page}", name="admin_ads_index",requirements={"page":"\d+"})
     * expression regulier pour dire du on peut avoir plus de 2 chiffres
     * on peut faire autrement dans la route /admin/ads/{page<\d+>?1}
     * ?===optionnel et il faut supprimer =1 dans $page=1
     */
    public function index(AdRepository $repo,$page=1,Paginator $paginator)
    {
        // $limit=10;
        // $start=$page*$limit-$limit;
        // //      1*10-10=0
        // //      3*10-10=20
        $paginator->setEntityClass(Ad::class)
                ->setCurentPage($page);
                

         $ads=$paginator->getData();        
        // $total=count( $repo->findAll() );
        // $pages=ceil( $total/10 );//3.2=4
        $pages=$paginator->getPages();
        
        return $this->render('admin/ad/index.html.twig', [
            'ads'=>$ads,
            'pages'=>$pages,
            'page'=>$page//parametre de la route
        ]);
    }
    /**
     * @Route("/admin/ads/{id}/edit",name="admin_ads_edit")
     */
    public function edit(Ad $ad,Request $request,EntityManagerInterface $manager){
        $form=$this->createForm(AdType::class,$ad);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($ad);
            $manager->flush();
            
            $this->addFlash("success", "L'annonce a ete modifié avec success");
           
        }
        
        return $this->render("admin/ad/edit.html.twig",[
            'ad'=>$ad,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/ads/{id}/delete",name="admin_ads_delete")
     * permet a l'admin de supprimer une annonce
     */
    public function delete(Ad $ad,EntityManagerInterface $manager){
        
        if(count($ad->getBookings()) > 0){
            $this->addFlash('warning','Vous ne pouvez pas la supprimer
            car elle possede deja des reservations');
        }else{
        $manager->remove($ad);
        $manager->flush();
        $this->addFlash('success',"L'annonce a été supprimé avec succes");
    }
        return $this->redirectToRoute("admin_ads_index");
    }

}
