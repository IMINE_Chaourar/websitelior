<?php
namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Booking;
use App\Entity\Comment;
use App\Form\BookingType;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingController extends AbstractController
{

    /**
     * Permet de reserver une annonce
     *
     * @Route("/ads/{slug}/book", name="booking_create")
     * @IsGranted("ROLE_USER")
     */
    public function index(Ad $ad, Request $request, EntityManagerInterface $manager)
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking,[
            'validation_groups'=>["front","Default"]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $booking->setBooker($user)
                    ->setAd($ad);
                    
            //si les dates ne sont pas dispo
                    if(!$booking->isBookableDates()){
                        $this->addFlash("warning",
                            "la date choisie est deja prise");
                    }
                    
            //sinon   
                    else{
            $manager->persist($booking);
            $manager->flush();
            return $this->redirectToRoute("booking_show",[
                'id'=>$booking->getId(),
                'withAlert'=>true
            ]);
        }
        }

        return $this->render('booking/book.html.twig', [
            'form' => $form->createView(),
            'ad' => $ad
        ]);
    }
    
    
    
    
    
    
    
    /**
     * Permet d'afficher la page d'une reservation
     * @Route("/booking/{id}" ,name="booking_show")
     */
    public function showB(Booking $booking,Request $request,EntityManagerInterface $manager){
        $comment=new Comment();

        $form=$this->createForm(CommentType::class,$comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $comment->setAd($booking->getAd())
                    ->setAuthor($this->getUser());

            $manager->persist($comment);
            $manager->flush();
            $this->addFlash('success',"Votre a bien été pris en compte");
            
        }

        
        return $this->render('booking/show.html.twig',[
           'booking'=>$booking ,
           'form'=>$form->createView()
        ]);
    }
    
    
    
    
    
    
}
