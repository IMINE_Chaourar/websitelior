<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\User;
use App\Entity\Booking;
use App\Entity\Comment;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\AdRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields={"title"},
 * message="une autre annonce possede deja ce titre !!!!!"
 * )
 */
class Ad
{

    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     * min=5,max=255,minMessage="Le titre doit faire plus de 5 caracteres",
     * maxMessage="Le titre ne peux pas faire plus de 255 caracteres"
     * )
     */
    private $title;

    /**
     *
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     *
     * @ORM\Column(type="text")
     */
    private $introduction;

    /**
     *
     * @ORM\Column(type="text")
     * @Assert\Length(min=20,minMessage="Votre description doit faire plus de 20 caracteres")
     */
    private $content;

    /**
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Url
     *
     */
    private $coverImage;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $rooms;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="ad", orphanRemoval=true)
     * @Assert\Valid()
     */
    private $images;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="ad")
     */
    private $bookings;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="ad", orphanRemoval=true)
     */
    private $comments;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * permet d'obtenir un tableau des jours qui ne sont pas dispo
     */
    public function getNotAvailableDates()
    {
        $notAvailableDates = [];

        foreach ($this->bookings as $booking) {
            // calculer les jours qui se trouvent entre l'arrivé et le depart
            $resultat = range($booking->getStartDate()->getTimestamp(), $booking->getEndDate()->getTimestamp(), 24 * 60 * 60);

            $days = array_map(function ($dayTimestamp) {
                return new \DateTime(date('Y-m-d', $dayTimestamp));
            }, $resultat);

            $notAvailableDates = array_merge($notAvailableDates, $days);
        }

        return $notAvailableDates;
    }


    /**
     * permet de recuperer 
     * le commantaire d'un author par rapport a une annonce
     */
    public function getCommentFromAuthor(User $author){
        foreach($this->comments as $comment){
            if ($comment->getAuthor() === $author) 
               return $comment;
        }
        return null;
    }


    /*
     * permet dafficher la moyenne des note
     */
    public function getAvgRatings()
    {
        // calculer la some des notes
        $comments = $this->comments->toArray();
        $a=[];
        foreach ($comments as $comment) {
            
            $a[] = $comment->getRating();
            
        }
        $noteSum = array_sum($a);

        // //calculer la moyenne des notes

        if (count($this->comments) > 0)
            return $noteSum / count($this->comments);
        else
            return 0;
    }

    /**
     * permet d'initialiser le slug a partir de l'entity
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     */
    public function initialiseSlug()
    {
        if (empty($this->slug)) {
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCoverImage(): ?string
    {
        return $this->coverImage;
    }

    public function setCoverImage(string $coverImage): self
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    public function getRooms(): ?int
    {
        return $this->rooms;
    }

    public function setRooms(int $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     *
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (! $this->images->contains($image)) {
            $this->images[] = $image;
            $image->setAd($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getAd() === $this) {
                $image->setAd(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     *
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (! $this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setAd($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getAd() === $this) {
                $booking->setAd(null);
            }
        }

        return $this;
    }

    /**
     *
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (! $this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAd($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAd() === $this) {
                $comment->setAd(null);
            }
        }

        return $this;
    }
}
