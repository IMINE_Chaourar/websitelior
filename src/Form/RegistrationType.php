<?php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class RegistrationType extends ApplicationType
{

    

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, $this->getconfig("Nom", "Nom..."))
            ->add('lastName', TextType::class, $this->getconfig("Prenom", "Prenom..."))
            ->add('email', EmailType::class, $this->getconfig("Email", "Adresse email"))
            ->add('picture', UrlType::class, $this->getconfig("photo de profil", "Url de votre avatar"))
            ->add('hash', PasswordType::class, $this->getconfig("Mot de passe", "Mot de passe"))
            ->add('confirmPassword',PasswordType::class,$this->getconfig("Confirmer le password", "Confirmer le password"))
            ->add('introduction', TextType::class, $this->getconfig("Introduction", "Presentez vous en quelques mots"))
            ->add('description', TextareaType::class, $this->getconfig("Description", "C'est le moment de vous presentez en details "));
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
