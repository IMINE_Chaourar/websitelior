<?php
namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdType extends ApplicationType
{

    

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, $this->getconfig('Titre', 'Titre de votre annonce'))
            ->add('slug', TextType::class, $this->getconfig('Adresse web', 'Taper l\'adresse web', [
            'required' => false
        ]))
            ->add('coverImage', UrlType::class, $this->getconfig('URL de l\'image principale', 'Donnezl URL d\'une image qui donne vraiment envie '))
            ->add('introduction', TextType::class, $this->getconfig('Introduction', 'Donnez une description globale de votre annonce'))
            ->add('content', TextType::class, $this->getconfig('Description', 'Tapez une description qui donne vraiment envie de venir chez vous'))
            ->add('rooms', IntegerType::class, $this->getconfig('Nombre de chambres', 'Indiquez le nombre de chambres disponible'))
            ->add('price', MoneyType::class, $this->getconfig('Prix par nuit', 'Indiquer le prix que vous voulez pour une nuit'))
            ->add('images', CollectionType::class, [
            'entry_type' => ImageType::class,
            'allow_add' => true,
            'allow_delete' => true
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class
        ]);
    }
}
