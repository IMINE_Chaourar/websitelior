<?php
namespace App\Form;

use App\Entity\Booking;
use App\Form\DataTransformer\FrenshToDateTimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class BookingType extends ApplicationType
{
    private $transformer;
    public function __construct(FrenshToDateTimeTransformer $transformer){
        $this->transformer=$transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('startDate', TextType::class, $this->getconfig("Date d'arrivée", "Date d'arrivée"))
            ->add('endDate', TextType::class, $this->getconfig("Date de depart", "Date de depart"))
            ->add("comment", TextareaType::class, $this->getconfig(false, "Si vous avez un commantaire n'hesitez pas a me faire part !", [
            "required" => false
        ]));
            
          $builder->get('startDate')->addModelTransformer($this->transformer)  ;
          $builder->get('endDate')->addModelTransformer($this->transformer)  ;
            
    }

    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'validation_groups'=>["front","Default"]
        ]);
    }
}
