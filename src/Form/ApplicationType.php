<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType
{
    /**
     * permet d'avoir la configuration de base d'un champs
     *
     * @param string $label
     * @param string $placeholder
     * @param array $option
     * @return array
     */
    protected  function getconfig($label, $placeholder, $option = [])
    {
        return array_merge_recursive([
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder,
                
            ]
        ], $option);
    }
    
}

