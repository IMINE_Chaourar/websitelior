<?php
namespace App\DataFixtures;

use App\Entity\Ad;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Role;
use App\Entity\Booking;
use App\Entity\Comment;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $manager->persist($adminRole);

        $adminUser = new User();
        $adminUser->setFirstName("Chaourar")
            ->setLastName("IMINE")
            ->setEmail("imine.chaourar07@gmail.com")
            ->setHash($this->encoder->encodePassword($adminUser, "1234567"))
            ->setPicture("https://www.facebook.com/photo.php?fbid=2440656236178003&set=a.1381167635460207&type=3&theater")
            ->setIntroduction("rien a dire le plus grand faineant dans cette planete !!!!!!!!!!")
            ->setDescription("rien a dire le plus grand faineant dans cette planete !!!!!!!!!!")
            ->addUserRole($adminRole);
        $manager->persist($adminUser);

        $users = [];
        $genres = [
            'male',
            'female'
        ];

        // nous gerons les Users
        for ($i = 0; $i < 10; $i ++) {
            $genre = $faker->randomElement($genres);
            $picture = 'https://randomuser.me/api/portraits/';
            $pictureId = $faker->numberBetween(1, 99) . '.jpg';
            if ($genre == 'male')
                $picture .= "men/" . $pictureId;
            else
                $picture .= "women/" . $pictureId;

            $user = new User();

            $hash = $this->encoder->encodePassword($user, 'password');

            $user->setFirstName($faker->firstName($genre))
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setIntroduction($faker->sentence())
                ->setDescription('<p>' . join('</p><p>', $faker->paragraphs(3)) . '</p>')
                ->setHash($hash)
                ->setPicture($picture);
            $manager->persist($user);
            $users[] = $user;
        }

        // nous gerons les annonces
        for ($i = 0; $i <= 30; $i ++) {

            $ad = new Ad();

            $titre = $faker->sentence();

            $coverImage = $faker->imageUrl(1000, 350);
            $introduction = $faker->paragraph(2);
            $content = '<p>' . join('</p><p>', $faker->paragraphs(5)) . '</p>';

            $user = $users[mt_rand(0, count($users) - 1)];

            $ad->setTitle($titre)
                ->setCoverImage($coverImage)
                ->setIntroduction($introduction)
                ->setContent($content)
                ->setPrice(mt_rand(40, 200))
                ->setRooms(mt_rand(1, 6))
                ->setAuthor($user);

            for ($j = 0; $j <= mt_rand(2, 5); $j ++) {
                $image = new Image();
                $image->setUrl($faker->imageUrl())
                    ->setCaption($faker->sentence())
                    ->setAd($ad);
                $manager->persist($image);
            }

            // Gestion des reservations
            for ($j = 1; $j <= mt_rand(0, 10); $j ++) {
                $booking = new Booking();

                $createdAt = $faker->dateTimeBetween('-6 months');
                $startDate = $faker->dateTimeBetween('-3 months');
                $duration = mt_rand(3, 10);
                $endDate = (clone $startDate)->modify("+$duration days");
                $amount = $ad->getPrice() * $duration;
                $booker = $users[mt_rand(0, count($users) - 1)];
                $comment = $faker->paragraph();

                $booking->setAd($ad)
                    ->setStartDate($startDate)
                    ->setEndDate($endDate)
                    ->setBooker($booker)
                    ->setCreatedAt($createdAt)
                    ->setAmount($amount)
                    ->setComment($comment);

                $manager->persist($booking);

                // Gestion des commentaires
                if (mt_rand(0, 1)) {
                    $commentBooking = new Comment();
                    $commentBooking->setContent($faker->paragraph())
                                   ->setRating(mt_rand(1,5))
                                   ->setAuthor($booker)
                                   ->setAd($ad);
                                   $manager->persist($commentBooking);
                }
            }

            $manager->persist($ad);
        }

        $manager->flush();
    }
}
